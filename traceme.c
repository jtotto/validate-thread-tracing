#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define SPIN_COUNTDOWN (1UL << 28)
#define SPIN_ROLLOVER 8
#define NUM_THREADS 32
#define OUT_STREAM stdout

static int current_spin;

static inline void spin(void)
{
    volatile uint64_t countdown = SPIN_COUNTDOWN;
    for ( ; countdown; countdown--);
}

static void *spin_thread_fn(void *arg)
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    fprintf(OUT_STREAM, "%lld.%.9ld created new spinthread\n",
            (long long)ts.tv_sec, ts.tv_nsec);
    int *cur_spin = arg;
    int read_spin = __sync_add_and_fetch(cur_spin, 1);
    if (read_spin >= SPIN_ROLLOVER)
        __sync_and_and_fetch(cur_spin, 0);

    for (int i = 0; i < read_spin; i++)
        spin(); /* don't sleep - don't want a context switch */

    clock_gettime(CLOCK_REALTIME, &ts);
    fprintf(OUT_STREAM, "%lld.%.9ld syscall sleep in spinthread\n",
            (long long)ts.tv_sec, ts.tv_nsec);
    /* NOW I want a context switch */
    sleep(read_spin / 2);

    clock_gettime(CLOCK_REALTIME, &ts);
    fprintf(OUT_STREAM, "%lld.%.9ld finishing spinthread\n",
            (long long)ts.tv_sec, ts.tv_nsec);

    return NULL;
}

int main(void)
{
    current_spin = 0;
    __sync_add_and_fetch(&current_spin, 1);

    /* repeatedly create a bunch of threads and then wait for them */
    while (1) {
        pthread_t spinthreads[NUM_THREADS];
        for (int i = 0; i < NUM_THREADS; i++) {
            pthread_create(&spinthreads[i], NULL, spin_thread_fn,
                           &current_spin);
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            pthread_join(spinthreads[i], NULL);
        }
    }

    return 0;
}
